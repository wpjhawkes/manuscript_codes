#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan  5 15:18:05 2021

@author: will.hawkesultromics.com
"""


# logistic regression
def logistic_regression(db, columns, dep_var, scaled, test_model = False):
    import pandas as pd
    from sklearn.metrics import roc_curve
    from sklearn.metrics import roc_auc_score
    import numpy as np
    from matplotlib import pyplot as plt
    from sklearn.preprocessing import StandardScaler
    import statsmodels.api as sm
    
    
    def logit_performance(cm):
        sensitivity = cm[1,1]/ (cm[1,1] + cm[1,0])
        specificity = cm[0,0]/ (cm[0,0] + cm[0,1])
        ppv = cm[1,1] / (cm[1,1] + cm[0,1])
        npv = cm[0,0] / (cm[0,0] + cm[1,0])
    
        return sensitivity, specificity, ppv, npv
    
    if len(columns) == 1:
        print(columns)
    
    # drop nans
    db = db[columns + [dep_var]].dropna()
    
    #setting up X and y
    X= db[columns].dropna()
    y = db.loc[list(X.index),dep_var]
    
    if scaled == True:
        scaler = StandardScaler()
        scaler.fit(X)
        X_scaled = pd.DataFrame(scaler.transform(X), index = X.index, columns = X.columns)
        X = X_scaled
    
    
    
    if len(columns) >1:
        #  calculate variance inflation factor for independent variables
        from statsmodels.stats.outliers_influence import variance_inflation_factor 
      
        # VIF dataframe 
        vif_data = pd.DataFrame() 
        vif_data["feature"] = X.columns
          
        # calculating VIF for each feature 
        vif_data["VIF"] = [variance_inflation_factor(X.values, i) for i in range(len(X.columns))] 
          
        print(vif_data)
        
    n = len(X)
    print('sample size = ', n)
    #filling in the statsmodels Logit method
    X = sm.add_constant(X)
    logit_model = sm.Logit(y,X)
    result = logit_model.fit(maxiter = 100) #max iteratiosn matched to sklearn model
    # print(result.summary())
    # print(np.exp(result.params))
    # print(np.exp(result.conf_int()))
    
    ORs = np.exp(result.params).round(3)
    ci = np.exp(result.conf_int()).round(3)
    pval = result.pvalues.round(3)
    
    # for i in range(0,len(columns)):
    #     col = columns[i]
    #     or_ci = columns[i] + ' ' + str(np.exp(result.params)[col].round(2)) + ' (' + str(np.exp(result.conf_int()).loc[col,0].round(2)) + '-' + str(np.exp(result.conf_int()).loc[col,1].round(2)) + ') ' + str(result.pvalues[col].round(3))
    #     data.append(or_ci)
    #     print(or_ci)
    
    
        
    
    performance = None
    if test_model == True:
        # test model performance
        from sklearn.linear_model import LogisticRegression
        
        y = y.values.ravel()
        logreg = LogisticRegression()
        logreg.fit(X, y)
        
        logit_roc_auc = roc_auc_score(y, logreg.predict(X))
        fpr, tpr, thresholds = roc_curve(y, logreg.predict_proba(X)[:,1])
        # plt.figure()
        # plt.plot(fpr, tpr, label='Logistic Regression (area = %0.2f)' % logit_roc_auc)
        # plt.plot([0, 1], [0, 1],'r--')
        # plt.xlim([0.0, 1.0])
        # plt.ylim([0.0, 1.05])
        # plt.xlabel('False Positive Rate')
        # plt.ylabel('True Positive Rate')
        # plt.title('Receiver operating characteristic')
        # plt.legend(loc="lower right")
        plt.show()
        print(logit_roc_auc)
        
        # get the Standard Error for the AUROC to calculate 95% ci's
        n1 = len(db[db[dep_var] == 0])
        n2 = len(db[db[dep_var] == 1])
        q0 = logit_roc_auc * (1-logit_roc_auc)
        q1 = logit_roc_auc / (2-logit_roc_auc) - logit_roc_auc**2
        q2 = 2 * logit_roc_auc**2 / (1 + logit_roc_auc) - logit_roc_auc**2
        se = np.sqrt((q0 + (n1-1) * q1 + (n2 - 1) * q2) / (n1 * n2))
        
        sensitivity, specificity, ppv, npv = logit_performance(result.pred_table())
        performance = {'aic' : result.aic.round(3), 'auroc' : logit_roc_auc.round(3), 'auroc_ll' : (logit_roc_auc - 1.95 * se).round(3), 'auroc_ul' : (logit_roc_auc + 1.95 * se).round(3), 'sensitivity' : sensitivity.round(3), 'specificity' : specificity.round(3), 'ppv' : ppv.round(3), 'npv' : npv.round(3)}
    
    return result, ORs, ci, pval, performance, n
  
 
def forward_regression(X, y,
                       threshold_in,
                       verbose=True):
    
    import statsmodels.api as sm
    import pandas as pd
    
    initial_list = []
    included = list(initial_list)
    while True:
        changed=False
        excluded = list(set(X.columns)-set(included))
        new_pval = pd.Series(index=excluded)
        for new_column in excluded:
            model = sm.OLS(y, sm.add_constant(pd.DataFrame(X[included+[new_column]]))).fit()
            new_pval.loc[new_column] = model.pvalues[new_column]
        best_pval = new_pval.min()
        if best_pval < threshold_in:
            best_feature = new_pval.idxmin()
            included.append(best_feature)
            changed=True
            if verbose:
                print('Add  {:30} with p-value {:.6}'.format(best_feature, best_pval))

        if not changed:
            break

    return included


# forwradstepwise regression featyure selection
def logreg_fsw(variables, outcome, threshold, scale):
    from sklearn.preprocessing import StandardScaler
    import pandas as pd
    
    variables = variables.dropna() 
    X = variables.drop(outcome, axis = 1)
    Y = variables[outcome].values.ravel()
    
    if scale == True:
        # scale data 
        scaler = StandardScaler()
        scaler.fit(X)
        X_scaled = pd.DataFrame(scaler.transform(X), index = X.index, columns = X.columns)
        X = X_scaled
        
    
    
    features = forward_regression(X, Y, threshold)
    
    return features


def bootstrap_logistic_regression(db, columns, dep_var, scaled, iterations):
    import pandas as pd
    from sklearn.metrics import roc_curve
    from sklearn.metrics import roc_auc_score
    import numpy as np
    from matplotlib import pyplot as plt
    from sklearn.preprocessing import StandardScaler
    import statsmodels.api as sm
    import numpy
    from pandas import read_csv
    from sklearn.utils import resample
    from sklearn.tree import DecisionTreeClassifier
    from sklearn.metrics import accuracy_score
    from matplotlib import pyplot
    
    
    def logit_performance(cm):
        sensitivity = cm[1,1]/ (cm[1,1] + cm[1,0])
        specificity = cm[0,0]/ (cm[0,0] + cm[0,1])
        ppv = cm[1,1] / (cm[1,1] + cm[0,1])
        npv = cm[0,0] / (cm[0,0] + cm[1,0])
    
        return sensitivity, specificity, ppv, npv

    #setting up X and y
    X= db[columns].dropna()
    y = db.loc[list(X.index),dep_var]
    
    if scaled == True:
        scaler = StandardScaler()
        scaler.fit(X)
        X_scaled = pd.DataFrame(scaler.transform(X), index = X.index, columns = X.columns)
        X = X_scaled
    
    if len(columns) >1:
        #  calculate variance inflation factor for independent variables
        from statsmodels.stats.outliers_influence import variance_inflation_factor 
      
        # VIF dataframe 
        vif_data = pd.DataFrame() 
        vif_data["feature"] = X.columns 
          
        # calculating VIF for each feature 
        vif_data["VIF"] = [variance_inflation_factor(X.values, i) 
                                  for i in range(len(X.columns))] 
          
        print(vif_data)
    
    #filling in the statsmodels Logit method
    X = sm.add_constant(X)
    logit_model = sm.Logit(y,X)
    result = logit_model.fit(maxiter = 100) #max iteratiosn matched to sklearn model
    print(result.summary())
    # print(np.exp(result.params))
    # print(np.exp(result.conf_int()))
    
    data = []
    for i in range(0,len(columns)):
        col = columns[i]
        or_ci = columns[i] + ' ' + str(np.exp(result.params)[col].round(2)) + ' (' + str(np.exp(result.conf_int()).loc[col,0].round(2)) + '-' + str(np.exp(result.conf_int()).loc[col,1].round(2)) + ') ' + str(result.pvalues[col].round(3))
        data.append(or_ci)
        print(or_ci)
    
    # test model performance
    from sklearn.linear_model import LogisticRegression
    from sklearn.model_selection import train_test_split
    from sklearn.metrics import confusion_matrix


    y = y.values.ravel()
    
    
    # configure bootstrap
    n_iterations = iterations
    
    # run bootstrap
    aic, aurocs, sensitivities, specificities, ppvs, npvs = list(), list(), list(), list(), list(), list()
    
    for i in range(n_iterations):
        # prepare train and test sets
        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3)
        
        # fit model
        model = LogisticRegression()
        model.fit(X_train, y_train)
        # evaluate model
        # predictions = model.predict(X_test)
        # score = accuracy_score(test[:,-1], predictions)
        # print(score)
        logit_roc_auc = roc_auc_score(y_test, model.predict(X_test))
        y_pred = model.predict(X_test)
        cm = confusion_matrix(y_test, y_pred)
        sensitivity, specificity, ppv, npv = logit_performance(cm)
        aurocs.append(logit_roc_auc)
        sensitivities.append(sensitivity)
        specificities.append(specificity)
        ppvs.append(ppv)
        npvs.append(npv)
        # aic.append(model.aic.round(3))
        
 
    # confidence intervals
    def get_confidence(iterations):
        alpha = 0.95
        p = ((1.0-alpha)/2.0) * 100
        lower = max(0.0, numpy.percentile(iterations, p)).round(3)
        p = (alpha+((1.0-alpha)/2.0)) * 100
        upper = min(1.0, numpy.percentile(iterations, p)).round(3)
        return lower, upper
    
    
    
    performance = { 'auroc' : (sum(aurocs)/ len(aurocs)).round(3), 'auroc_ci' : get_confidence(aurocs), 'aurocs' : aurocs, 'sensitivity' : sum(sensitivities)/ len(sensitivities), 'sensitivity_cis' : get_confidence(sensitivities), 'sensitivities' : sensitivities, 'specificity' : sum(specificities)/ len(specificities), 'specificity_cis' : get_confidence(specificities), 'specificities' : specificities, 'ppv' : sum(ppvs)/ len(ppvs), 'ppv_cis' : get_confidence(ppvs), 'ppvs' : ppvs, 'npv' : sum(npvs)/ len(npvs), 'npv_cis' : get_confidence(npvs), 'npvs' : npvs}
    
    return result, data, performance