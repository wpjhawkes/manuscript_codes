#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 20 10:30:41 2021

@author: will.hawkesultromics.com
"""


def RD_535_data_prep(database, eva, eva_outcomes, ohsu, ohsu_outcomes, volumes):

    import pandas as pd
    from datetime import timedelta
        
    outcomes = pd.concat([eva_outcomes, ohsu_outcomes], axis = 0)
    
    database = database.join(outcomes, how= 'left')
    
    # calculate time from SE to angio where applicable, time in days
    ohsu['time'] = (pd.to_datetime(ohsu.ANGDT, dayfirst = True, errors = 'coerce') - pd.to_datetime(ohsu.DOV, dayfirst = True, errors = 'coerce'))/ timedelta(days = 1)
    eva['time'] = (pd.to_datetime(eva.Angiogram_Date, dayfirst = True, errors = 'coerce') - pd.to_datetime(eva.SE_Date, dayfirst = True, errors = 'coerce'))/ timedelta(days = 1)
    time_lookup = pd.concat([eva.time, ohsu.time], axis = 0).dropna()
    
    # get SE interpretation data
    ohsu.loc[ohsu['OSER#Abnormal'] == 0, 'se_interp'] = 0
    ohsu.loc[ohsu['OSER#Abnormal'] == 1, 'se_interp'] = 1
    eva.loc[eva['Clinician_Interpretation'] == 0, 'se_interp'] = 0
    eva.loc[eva['Clinician_Interpretation'] == 1, 'se_interp'] = 1
    se_interpretation = pd.concat([ohsu.se_interp, eva.se_interp], axis = 0).dropna()
    
   
    volumes['delta_ESV'] = volumes.vol_ES_R - volumes.vol_ES_P
    database = database.join(volumes, how = 'left')
    
    # calculate the delta WMSI
    database['delta_rwmsi'] = database.PWMSI - database.RWMSI
    
    # add time and se interpretation to the database
    database = database.join(time_lookup, how = 'left') 
    database = database.join(se_interpretation, how = 'left')
    
    
    # Create GLS and EF Variables
    database.loc[database.GLS_P <=-17, 'p_gls_co'] = 0
    database.loc[database.GLS_P >-17, 'p_gls_co'] = 1
    database.loc[database.EF_P <64, 'p_ef_co'] = 1
    database.loc[database.EF_P >=64, 'p_ef_co'] = 0
    database.loc[database.GLS_R <=-16, 'r_gls_co'] = 0
    database.loc[database.GLS_R >-16, 'r_gls_co'] = 1
    database.loc[database.EF_R <56, 'r_ef_co'] = 1
    database.loc[database.EF_R >=56, 'r_ef_co'] = 0
    database.loc[database.GLS_diff <=-3.67, 'd_gls_co'] = 0
    database.loc[database.GLS_diff >-3.67, 'd_gls_co'] = 1
    database.loc[database.EF_diff <-3.5, 'd_ef_co'] = 0
    database.loc[database.EF_diff >=-3.5, 'd_ef_co'] = 1
    database.loc[database.GLS_diff >=-3.4, 'diff_gls_co'] = 0
    database.loc[database.GLS_diff <-3.4, 'diff_gls_co'] = 1
    database['pef_pgls'] = 0 
    database.loc[(database.GLS_P >-17) & (database.EF_P < 64), 'pef_pgls'] = 1
    
    # Create SE Variables
    database.loc[database.PWMSI > 1, 'PWMSI>1'] = 1
    database.loc[database.PWMSI == 1, 'PWMSI>1'] = 0
    database.loc[database.PWMSI >= 1.12, 'PWMSI>1_12'] = 1
    database.loc[database.PWMSI < 1.12, 'PWMSI>1_12'] = 0
    database.loc[database.PWMSI >= 1.18, 'PWMSI>1_18'] = 1
    database.loc[database.PWMSI < 1.18, 'PWMSI>1_18'] = 0
    database.loc[database['Number of Ischaemic segments'] != 1, '1segments'] = 0
    database.loc[database['Number of Ischaemic segments'] == 1, '1segments'] = 1
    database.loc[database['Number of Ischaemic segments'] != 2, '2segments'] = 0
    database.loc[database['Number of Ischaemic segments'] == 2, '2segments'] = 1
    database.loc[database['Number of Ischaemic segments'] != 3, '3segments'] = 0
    database.loc[database['Number of Ischaemic segments'] == 3, '3segments'] = 1
    database.loc[database['Number of Ischaemic segments'] != 4, '4segments'] = 0
    database.loc[database['Number of Ischaemic segments'] == 4, '4segments'] = 1
    database.loc[database['Number of Ischaemic segments'] != 5, '5segments'] = 0
    database.loc[database['Number of Ischaemic segments'] == 5, '5segments'] = 1
    database.loc[database['delta_rwmsi'] <= 0.37, 'PWMSI - RMWSI'] = 0
    database.loc[database['delta_rwmsi'] > 0.37, 'PWMSI - RMWSI'] = 1
    
    # database['PWMSI + GLS'] = 0
    database.loc[(database['PWMSI>1'] == 1) & (database.p_gls_co == 1), 'PWMSI + GLS'] = 1
    # database['PWMSI>1_12 + GLS'] = 0
    database.loc[(database['PWMSI>1_12'] ==1) & (database.p_gls_co == 1), 'PWMSI>1_12 + GLS'] = 1
    # database['PWMSI>1_18 + GLS'] = 0
    database.loc[(database['PWMSI>1_18'] == 1) & (database.p_gls_co == 1), 'PWMSI>1_18 + GLS'] = 1
    # database['Ischaemia + GLS'] = 0
    database.loc[(database.Ischaemic == 1) & (database.p_gls_co==1), 'Ischaemia + GLS'] = 1
    # database['>3 Segments + GLS'] = 0
    database.loc[(database['3segments'] == 1) & (database.p_gls_co==1), '>3 Segments + GLS'] = 1
    # database['>3 Segments + delta_GLS'] = 0
    database.loc[(database['3segments'] == 1) & (database.diff_gls_co==1), '>3 Segments + delta_GLS'] = 1
    # database['>4 Segments + GLS'] = 0
    database.loc[(database['4segments'] == 1) & (database.p_gls_co==1), '>4 Segments + GLS'] = 1
    # database['delta_WMSI + GLS'] = 0
    database.loc[(database['PWMSI - RMWSI'] == 1) & (database.p_gls_co==1), 'delta_WMSI + GLS'] = 1
    # database['PWMSI + EF'] = 0
    database.loc[(database.PWMSI > 1) & (database.p_ef_co == 1), 'PWMSI + EF'] = 1
    # database['Ischaemia + EF'] = 0
    database.loc[(database.Ischaemic == 1) & (database.p_ef_co==1), 'Ischaemia + EF'] = 1
    # database['>3 Segments + EF'] = 0
    database.loc[(database['3segments'] == 1) & (database.p_ef_co==1), '>3 Segments + EF'] = 1
    # database['delta_WMSI + EF'] = 0
    database.loc[(database['PWMSI - RMWSI'] == 1) & (database.p_ef_co==1), 'delta_WMSI + EF'] = 1
    # database['PWMSI>1_12 + EF'] = 0
    database.loc[(database.PWMSI > 1.12) & (database.p_ef_co == 1), 'PWMSI>1_12 + EF'] = 1
    
    # database['PWMSI>1_18 + delta_GLS'] = 0
    database.loc[(database['PWMSI>1_18'] ==1) & (database.diff_gls_co == 1), 'PWMSI>1_18 + delta_GLS'] = 1
    # database['Ischaemic + pef_pgls'] = 0
    database.loc[((database.Ischaemic == 1) & database.pef_pgls == 1), 'Ischaemic + pef_pgls'] = 1
    
    # identify unecessary procedures
    database.loc[(database.Outcome == 0) & (database.time.isna() == False), 'valid_angio'] = 0
    database.loc[(database.Outcome == 1) & (database.time.isna() == False), 'valid_angio'] = 1
    
    # add composite variables using z scores
        # create z score for continuous variables, variabels are scaled to SD's from the mean. This was done to overcome Convergance warnings for RFE 
    variables = ['Age', 'Gender', 'Height', 'Weight', 'BMI','EF_P', 'EF_R', 'EF_diff', 'GLS_P', 'GLS_R', 'GLS_diff']
    for i in variables:
        tag = str(i+'_z')
        database[tag] = (abs(database[i]) - abs(database[i].mean())) / abs(database[i].std())

    # composite variables for EF and GLS 
    database['gls_ef_z'] = database.GLS_P_z + database.EF_P_z
    
    
    # database.to_csv('/Users/will.hawkesultromics.com/Dropbox (Ultromics)/Ultromics'"'"'s shared workspace/Research Team/Technical Development/Jira Tasks/RD-535 Core SE Paper/full_database.csv')
    
    return database