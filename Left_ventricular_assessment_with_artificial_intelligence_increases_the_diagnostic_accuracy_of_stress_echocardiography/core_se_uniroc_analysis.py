#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 11 11:17:34 2021

@author: will.hawkesultromics.com
"""

from PythonStats.uniroc import rocfun

import pandas as pd

import matplotlib.pyplot as plt


import pandas as pd
import numpy as np
import statistics as stats
from matplotlib import pyplot as plt
from cycler import cycler
from sklearn.metrics import confusion_matrix


def rocfun(all_data,feat_lists, cols, co, Outcome, plot, ID):
    print(feat_lists)
    if ID != None:
        features_df = all_data.set_index(ID)
   # Initialise dataframes and variables
    features_df = all_data.loc[:,cols]
    

    outcome = features_df.loc[:,Outcome]
    if len(feat_lists) > 2:
        performance = pd.DataFrame({'feature' : feat_lists})
        
        statistics = pd.DataFrame({'feature' : feat_lists})
        
    if len(feat_lists) == 2:
        performance = pd.Series(feat_lists).to_frame()
        statistics = pd.Series(feat_lists).to_frame()
        
    
    
    # performance['youden'] = 0
    performance['co_direction'] = 0
    performance['auroc'] = 0
    performance['Sensitivity'] = 0
    performance['Specificity'] = 0
    performance['Accuracy'] = 0
    performance['ppv'] = 0
    performance['npv'] = 0
    
    
    statistics['HR_mean'] = 0
    statistics['HR_sd'] = 0
    statistics['LR_mean'] = 0
    statistics['LR_sd'] = 0
    
    performance = performance.rename(columns = {0:'feature'}).set_index('feature')
    statistics = statistics.rename(columns = {0:'feature'}).set_index("feature")
    performance.loc[:, 'cutoff'] = co
    data_dict = {}
    
    if plot == 'plot':
        fig, ax = plt.subplots()
    # loop to carry out simple ROC curve analysis for every feature
    for x in feat_lists:
        # print(x)
        # collect desired feature and patient outcomes
        data = features_df.loc[:,x]
        data = pd.concat([data,outcome], axis=1, sort=False).dropna()
        a = data.shape[0]
        b = int(a/2)
        # print(data[Outcome])
        HR_no = sum(data[Outcome])
        LR_no = a-HR_no
        
        
        # Sort data so that featrue values associated with high cad risk are at the top of the dataframe
        data = data.sort_values(x, ascending = False)
        cut = 0
        # if the prevous sort has placed high risk CAD values at the bottom of the dataframe, resort in ascending order
        if sum(data.iloc[0:b, 1]) < sum(data.iloc[b:a, 1]):
            data = data.sort_values(x, ascending=True)
            cut = 1
        
        # Calculate the cumulative sum of outcome positive patients as the script runs through the dataframe
        data['HR'] = data[Outcome].cumsum()
        
        # Calculate the cumulative sum of outcome negative patients.  Marks the row with a 1 everytime a 0 appears in "Outcome" then calculates rthe cumulative sum
        data['LR'] = 0        
        for i in range(0,(a-1)):
             if data.iloc[i,1] == 0:
                 data.iloc[i,3] = 1 
                 if data.iloc[i,1] == 1:
                     data.iloc[i,3] = 0
        data['LR'] = data['LR'].cumsum()
        
        
        data['Sensitivity'] = data.HR/HR_no
        data['Specificity'] = data.LR/LR_no 
        data['sum'] = data.Sensitivity +(1-(data.Specificity))
        

        # Calculate the maixmum sensitiivty and specificity
        max_sens_spec = max(data.Sensitivity +(1-(data.Specificity)))
        # youden = max(data.Sensitivity + data.Specificity -1)
        # performance.loc[x, 'youden'] = youden
        # Check if a cut-off exists. For use when you want to apply training cut-offs to vaidation dataset
        if performance.loc[x,'cutoff'] == 0 or performance.loc[x,'cutoff'] == np.nan:
            cut_off = data.loc[data['sum'] == max_sens_spec, x].iloc[0]
        elif performance.loc[x,'cutoff'] != 0:
            cut_off = performance.loc[x,'cutoff']

        
        roc = data.loc[:,[x,Outcome]]
        
        roc['decision'] = 0
       
        
        # Attempt to classify patients based on the optimum cut-off. 
        if cut == 0:
            for i in range(0,(a-1)):
                if roc.iloc[i,0] >= cut_off:
                    roc.iloc[i,2] = 1 
                elif roc.iloc[i,0] < cut_off:
                    roc.iloc[i,2] = 0
                       
        if cut == 1:
            for i in range(0,(a-1)):
                if roc.iloc[i,0] <= cut_off:
                    roc.iloc[i,2] = 1 
                elif roc.iloc[i,0] > cut_off:
                    roc.iloc[i,2] = 0
                        
        # Calculate AUROC using trapeziod method and evaluate performance        
        auroc = np.trapz(data['Sensitivity'],data['Specificity'])
        tn, fp, fn, tp = confusion_matrix(roc[Outcome], roc.decision).ravel()
        sensitivity = tp / (tp+fn)
        specificity = tn / (tn+fp)
        precision = tp / (tp+fp)
        accuracy = (tp+tn)/ data.shape[0]
        ppv = tp / (tp+fp)
        npv = tn / (tn+fn)
        f1 = (sensitivity+precision)/2
        harmonic = (2*sensitivity*specificity)/(sensitivity+specificity)
        
        #Create performance dataframe
        performance.loc[x,'cutoff'] = cut_off
        performance.loc[x,'auroc'] = auroc
        performance.loc[x,'Sensitivity'] = sensitivity
        performance.loc[x,'Specificity'] = specificity
        performance.loc[x,'Accuracy'] = accuracy    
        performance.loc[x,'ppv'] = ppv    
        performance.loc[x,'npv'] = npv
        performance.loc[x,'f1'] = f1
        performance.loc[x,'precision'] = precision
        performance.loc[x,'harmonic'] = harmonic
        
        if cut == 1:
            performance.loc[x,'co_direction'] = "<="
            
        elif cut == 0:
            performance.loc[x,'co_direction'] = ">="
        
        HR = data.loc[data[Outcome] == 1, [x]]
        HR_mean = stats.mean(HR[x])
        HR_sd = stats.stdev(HR[x])
        LR = data.loc[data[Outcome] == 0, [x]]
        LR_mean = stats.mean(LR[x])
        LR_sd = stats.stdev(LR[x])
        
        # Calcluate mean and SD of each biomarker
        statistics.loc[x,'HR_mean'] = HR_mean
        statistics.loc[x,'HR_sd'] = HR_sd
        statistics.loc[x,'LR_mean'] = LR_mean
        statistics.loc[x,'LR_sd'] = LR_sd
        
        # Plot ROC curves if needed
        # color = ['tab:orange','tab:pink','tab:cyan']
        # plt.figure()
        sensitivity = data.Sensitivity
        specificity = data.Specificity
        
        data_dict.update({x : data})
        if plot == 'plot':
            # # # plt.title(x)
            # plt.rcParams['figure.dpi'] = 150
            # plt.rcParams["font.weight"] = "normal"
            # plt.rcParams["axes.labelweight"] = "normal" 
            # if loops == 0:
            #     fig, ax = plt.subplots()
            # ax.set_aspect(0.8)
            # ax.plot(specificity,sensitivity, linewidth = 2, ls='-')
            # ax.grid(True)
            # ax.set_xlim([-0.01,1.01])
            # ax.set_ylim([-0.01,1.01])
            # ax.set_xlabel('1-Specificity', fontname = 'Arial', fontsize = 11)
            # ax.set_ylabel('Sensitivity', fontname = 'Arial', fontsize = 11)
            # # ax.legend(str(['Rect1 1, AUROC=','0.86, Sens=84%, Spec=79%','Solidity 1, AUROC=0.86, Sens=79%, Spec=82%','Solidity 2, AUROC=0.86, Sens=79%, Spec=82%'], loc=8, bbox_to_anchor=(0.5, -0.35), fontsize = 9)
            # # plt.tight_layout()
            # loops = loops + 1
            # Plot ROC curves if needed
            sensitivity = data.Sensitivity
            specificity = data.Specificity
            
            # plt.rcParams['figure.dpi'] = 300
            plt.ion()
            
            ax.plot(specificity,sensitivity, linewidth  = 2)
            # plt.title(x)
            ax.set_xlabel('1-Specificity', weight = 'bold', fontname = "Times New Roman")
            ax.set_ylabel('Sensitivity', weight = 'bold', fontname = "Times New Roman")
            ax.set_xlim(0, 1)
            ax.set_ylim(0, 1)
            for tick in ax.get_xticklabels():
                tick.set_fontname("Times New Roman")
            for tick in ax.get_yticklabels():
                tick.set_fontname("Times New Roman")
            ax.set_aspect('equal')
            
            plt.pause(0.1)
            plt.show()
    if plot == 'plot':        
        ax.legend(feat_lists, frameon = False)
        ax.plot([0, 1], [0, 1], transform=ax.transAxes, color = 'k', linestyle = '--')
        
    return performance, data_dict, performance.cutoff, statistics
    





# cols = ['ID','EF_R','EF_P', 'EF_diff','GLS_R','GLS_P','GLS_diff', 'Coronary Intervention']
# feat_list = ['EF_R','EF_P', 'EF_diff','GLS_R','GLS_P','GLS_diff']

# co = pd.DataFrame({'feature':feat_list})
# co = co.set_index('feature')
# co['cutoff'] = 0

# co.loc['GLS_P','cutoff'] = -17.4
# co.loc['EF_P','cutoff'] = 64

# uniroc_performance, uniroc_data, cutoff, statistics = rocfun(database_difficult_cases, feat_list, cols, co, 'Coronary Intervention', 'plot')


    
def plot_roc_curves(i, data_dict, pf, ax):
    
    # ef = 'tab:blue'
    # gls = 'tab:orange'  
    line_color = 'k'
    ef = 'k'
    gls = 'k'  
    peak = 'r'
    delta = 'b'
    rest = 'g'
    solid = 'solid'
    dash = (0, (3, 1))
    dots = (0, (1, 1))
    

    if '_R' in i and 'EF' in i:
        line_color = rest
        line_style = solid
        z = 1
    if '_P' in i and 'EF' in i:
        line_color = peak
        line_style = solid
        z=3
    if '_diff' in i and 'EF' in i:
        line_color = delta
        line_style = solid
        z=5
        
    if '_R' in i and 'GLS' in i:
        line_color = rest
        line_style = solid
        z=2
    if '_P' in i and 'GLS' in i:
        line_color = peak
        line_style = solid
        z=4
    if '_diff' in i and 'GLS' in i:
        line_color = delta
        line_style = solid
        z=6
        
    if i == 'RWMSI':
        line_color = rest
        line_style = solid
        z=1
        
    if i == 'delta_rwmsi':
         line_color = delta
         line_style = solid
         z=2
    
    if i == 'PWMSI':
        line_color = peak
        line_style = solid
        z=3

    ax.plot(data_dict[i].Specificity, data_dict[i].Sensitivity, color = line_color, linestyle = line_style, linewidth = 2, zorder = z)
    
    
    z = z+1
    x = 1 - pf.loc[i,'Specificity']
    y = pf.loc[i,'Sensitivity']
    # ax.scatter(x,y, marker = 'x', color = 'k', s = 60, linewidths =2, zorder = z)
    ax.scatter(x,y, marker = 'o', color = 'k', s = 30, linewidths =1, zorder = z)
    
    font_size = 12
    ax.set_xlabel('1-Specificity', weight = 'bold', fontname = "Times New Roman", size = font_size)
    ax.set_ylabel('Sensitivity', weight = 'bold', fontname = "Times New Roman", size = font_size)
    ax.set_xlim(0, 1)
    ax.set_ylim(0, 1)
    ax.tick_params(axis='both', labelsize = font_size)
    for tick in ax.get_xticklabels():
        tick.set_fontname("Times New Roman")
    for tick in ax.get_yticklabels():
        tick.set_fontname("Times New Roman")
    ax.set_aspect('equal')


    
# fig, ax = plt.subplots(figsize = (6,6))
# for i in ['EF_P','EF_diff', 'EF_R']:
#     plot_roc_curves(i, uniroc_data, uniroc_performance)
# plt.legend(['Peak Stress','Peak Stress - Resting', 'Resting'], loc = 'lower right', frameon = False)
# plt.savefig('/Users/will.hawkesultromics.com/Dropbox (Ultromics)/PythonCodes/ResearchCodes/RD_535_core_SE_paper/Figures_2022/EF_ROC', dpi = 300)

# fig, ax = plt.subplots(figsize = (6,6))
# for i in ['GLS_R','GLS_P','GLS_diff']:
#     plot_roc_curves(i, uniroc_data, uniroc_performance)
# plt.legend(['Peak Stress','Peak Stress - Resting', 'Resting'], loc = 'lower right', frameon = False)
# plt.savefig('/Users/will.hawkesultromics.com/Dropbox (Ultromics)/PythonCodes/ResearchCodes/RD_535_core_SE_paper/Figures_2022/GLS_ROC', dpi = 300)

# fig, ax = plt.subplots(figsize = (6,6))
# for i in ['PWMSI', 'delta_rwmsi', 'RWMSI']:
#     plot_roc_curves(i, uniroc_data, uniroc_performance)
# plt.legend(['Peak Stress','Peak Stress - Resting', 'Resting'], loc = 'lower right', frameon = False)
# plt.savefig('/Users/will.hawkesultromics.com/Dropbox (Ultromics)/PythonCodes/ResearchCodes/RD_535_core_SE_paper/Figures_2022/WMSI_ROC', dpi = 300)