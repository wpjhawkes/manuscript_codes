#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 20 17:23:53 2022

@author: will.hawkesultromics.com
"""
import pandas as pd
from lifelines import KaplanMeierFitter, CoxPHFitter
from lifelines.plotting import add_at_risk_counts
from ResearchCodes.RD_535_core_SE_paper.RD_535_functions import km_fitter, log_rank_test_survival
from itertools import combinations
from lifelines.statistics import logrank_test, multivariate_logrank_test
import matplotlib.pyplot as plt
import numpy as np

def km_multi_var(db, variable, splits, time, event, duration, censors = False, ci = True, leg = True):
    
    
    df = db[[variable, time, event]].dropna()
    if type(splits) == int:
        if splits == 2: # only use if data is already binary
        
            labelling = ['0', '1']
            bins = np.array([0, 1])
            df['categories'] = df[variable].astype(str)
        # df['categories'], bins = pd.qcut(df[variable], splits, labels = labelling, retbins = True)
        
        if splits == 3:
            labelling = ['T1', 'T2', 'T3']
            df['categories'], bins = pd.qcut(df[variable], splits, labels = labelling, retbins = True)
            
        if splits == 4:
            labelling = ['Q1', 'Q2', 'Q3', 'Q4']
            df['categories'], bins = pd.qcut(df[variable], splits, labels = labelling, retbins = True)
            
        
    
    if type(splits) == list and len(splits) > 2:
        labelling = []
        
        for i in range(0,len(splits)-1):
            # print(i)
            labelling.append(str(splits[i]) + ' to ' + str(splits[i+1]))
          
        df['categories'], bins = pd.cut(df[variable], splits, labels = labelling, retbins = True)
     
    bins = bins.round(1)   
    labels1 = df['categories'].unique()
    labels = [x for x in labels1 if str(x) != 'nan']
    print(labels)   
    
    data_dict = {}
    km_dict = {}
    for item in labelling:
        
        data_dict.update({item : df[df['categories'] == item]})
    
    for item in range(0, len(labelling)):

        km_dict.update({labelling[item] : {'kaplan_meier' : km_fitter(data_dict[labelling[item]][time], data_dict[labelling[item]][event], labelling[item]), 'data' : data_dict[labelling[item]]}})
                        # ,
                        # 'T2' : {'kaplan_meier' : km_fitter(data_dict[labelling[1]][time], data_dict[labelling[1]][event], labelling[1]), 'data' : data_dict[labelling[1]]}, 'T3' : {'kaplan_meier' : km_fitter(data_dict[labelling[2]][time], data_dict[labelling[2]][event], labelling[2]), 'data' : data_dict[labelling[2]]}})
    
    
    fig, ax = plt.subplots(figsize = (6,10))
    
    
    if splits == 2:
        km_dict['1']['kaplan_meier']['function'].plot(show_censors = censors, censor_styles={'ms': 6, 'marker': '+'}, ci_show= ci, ax = ax)
        km_dict['0']['kaplan_meier']['function'].plot(show_censors = censors, censor_styles={'ms': 6, 'marker': '+'}, ci_show= ci, ax = ax)
    else:
        for i in labelling:
        
            km_dict[i]['kaplan_meier']['function'].plot(show_censors = censors, censor_styles={'ms': 6, 'marker': '+'}, ci_show= ci, ax = ax)
        
    
    
    ax.set_xlim(0, duration)
    ax.set_ylim(0,1)
    
    if leg == True:
        ax.legend(frameon=False, bbox_to_anchor=(0.5, 1.2))  
    
    if len(labelling) == 3 and type(splits)!= list:
        add_at_risk_counts(km_dict['T1']['kaplan_meier']['function'], km_dict['T2']['kaplan_meier']['function'], km_dict['T3']['kaplan_meier']['function'], ax=ax)
        
    if len(labelling) == 4and type(splits)!= list:
        add_at_risk_counts(km_dict['Q1']['kaplan_meier']['function'], km_dict['Q2']['kaplan_meier']['function'], km_dict['Q3']['kaplan_meier']['function'], km_dict['Q4']['kaplan_meier']['function'], ax=ax)
    
    ax.set_ylim(0, 1)
    # Hide the right and top spines
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
     
    
    # Only show ticks on the left and bottom spines
    front_size = 12
    ax.yaxis.set_ticks_position('left')
    ax.xaxis.set_ticks_position('bottom')
    ax.set_xlabel('Time Since Echo (Days)', weight = 'bold', fontname = "Times New Roman", size = front_size)
    ax.set_ylabel('Freedom from CAD', weight = 'bold', fontname = "Times New Roman", size = front_size)
    ax.tick_params(axis='both', labelsize = front_size)
    
    for tick in ax.get_xticklabels():
        tick.set_fontname("Times New Roman")
    for tick in ax.get_yticklabels():
        tick.set_fontname("Times New Roman")
        
    plt.tight_layout()
    
    combs = list(combinations(labels, 2))
    
    log_rank = {}
    
    
    for pair in combs:
        # print(list(pair))
        # log_rank_results = {}
        print(pair[0])
        set1 = km_dict[pair[0]]['data']
        set1 = set1[set1[time] < duration]
        T1 = set1[time]
        E1 = set1[event]
        print(pair[1])
        set2 = km_dict[pair[1]]['data']
        set2 = set2[set2[time] < duration]
        T2 = set2[time]
        E2 = set2[event]
        
        # print(len(T1), len(T2), len(E1), len(E2))
        log_rank.update({pair : logrank_test(T1, T2, event_observed_A = E1, event_observed_B = E2).p_value})
     
        
        
    if len(labelling) > 2:
        result = multivariate_logrank_test(df[time], df['categories'], df[event])
        log_rank.update({'mv_logrank' : result})
        print('Multi-variate Log-Rank p = ', result.p_value)
        
    km_dict.update({'log_rank' : log_rank, 'bins' : bins})
    
    return km_dict