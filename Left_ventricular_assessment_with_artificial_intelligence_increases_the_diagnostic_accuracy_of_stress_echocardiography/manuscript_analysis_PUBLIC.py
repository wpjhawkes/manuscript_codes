#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 14 10:51:53 2022

@author: will.hawkesultromics.com
"""
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from core_se_paper.RegressionModels import logistic_regression,forward_regression, logreg_fsw, bootstrap_logistic_regression
from core_se_paper.RD_535_data_prep import RD_535_data_prep
from core_se_paper.core_se_uniroc_analysis import plot_roc_curves, rocfun
from core_se_paper.SurvivalAnalysis import km_multi_var
from lifelines.plotting import add_at_risk_counts
import seaborn as sns
from matplotlib.lines import Line2D

# load dataset from repo, enter location manually
database = database

database.loc[database['Number of Ischaemic segments'] >= 3, 'Number of Ischaemic segments'] = "\u2265 3"
database.loc[(database['Number of Ischaemic segments'] ==1) | (database['Number of Ischaemic segments'] ==2) , 'Number of Ischaemic segments'] = '1 to 2'

database.loc[(database['Number of Ischaemic segments']!= "\u2265 3"), '3ischaemic' ] = 0
database.loc[(database['Number of Ischaemic segments']== "\u2265 3"), '3ischaemic' ] = 1
# %% ROC curve Analysis
cols = ['Record ID','GLS_R','GLS_P','GLS_diff','EF_R','EF_P','EF_diff', 'RWMSI', 'PWMSI', 'delta_rwmsi', 'Outcome']
feat_list = ['GLS_R','GLS_P','GLS_diff','EF_R','EF_P','EF_diff', 'RWMSI', 'PWMSI', 'delta_rwmsi']

co = pd.DataFrame({'feature':feat_list})
co = co.set_index('feature')
co['cutoff'] = 0

uniroc_performance, uniroc_data, cutoff, statistics = rocfun(database.reset_index(drop = False), feat_list, cols, co, 'Outcome', 'plot', 'Record ID')

# obtain manucript plots

fig, ax = plt.subplots(figsize = (6,6))
for i in ['EF_P','EF_diff', 'EF_R']:
    plot_roc_curves(i, uniroc_data, uniroc_performance, ax)
plt.legend(['Peak LVEF','\u0394 LVEF', 'Rest LVEF'], loc = 'lower right', frameon = False)


fig, ax = plt.subplots(figsize = (6,6))
for i in ['GLS_P','GLS_diff','GLS_R']:
    plot_roc_curves(i, uniroc_data, uniroc_performance, ax)
plt.legend(['Peak GLS','\u0394 GLS', 'Rest GLS'], loc = 'lower right', frameon = False)


fig, ax = plt.subplots(figsize = (6,6))
for i in ['PWMSI', 'delta_rwmsi', 'RWMSI']:
    plot_roc_curves(i, uniroc_data, uniroc_performance, ax)
plt.legend(['Peak WMSI','\u0394 WMSI', 'Rest WMSI'], loc = 'lower right', frameon = False)



#%% figure 3 plot
fig, ax = plt.subplots()
sns.boxplot(x = 'Number of Ischaemic segments', y = 'GLS_P', data = database, hue = 'Outcome', order = [0.0, '1 to 2', "\u2265 3"])
custom_lines = [Line2D([0], [0], color='tab:blue', lw=4), Line2D([0], [0], color='tab:orange', lw=4)]
ax.legend(custom_lines, ['CAD Negative', 'CAD Positive'], frameon = False, bbox_to_anchor= [0.75, -0.13], ncol = 2)
ax.set_ylabel('GLS_P')
ax.set_xlabel('Number of Ischaemic Segments')
plt.tight_layout()

fig, ax = plt.subplots()
sns.boxplot(x = 'Number of Ischaemic segments', y = 'EF_P', data = database, hue = 'Outcome', order = [0.0, '1 to 2', "\u2265 3"])
custom_lines = [Line2D([0], [0], color='tab:blue', lw=4), Line2D([0], [0], color='tab:orange', lw=4)]
ax.legend(custom_lines, ['CAD Negative', 'CAD Positive'], frameon = False, bbox_to_anchor= [0.25, -0.13], ncol = 2)
ax.set_ylabel('EF_P')
ax.set_xlabel('Number of Ischaemic Segments')
plt.tight_layout()

# %% Figure 4 kaplan meier plots

# right censor negative patients at 365 days, make gls absolute for plotting consistency
database = database.fillna(value = {'time' : 365})
database.loc[:, 'GLS_P_neg'] = database['GLS_P'].copy()
database.loc[:, 'GLS_R_neg'] = database['GLS_R'].copy()
database.loc[:, ['GLS_P',  'GLS_R']] = database[['GLS_P', 'GLS_R']].abs()

# KM curves of peak ef with youden cutoff
var = 'EF_P'
km_ef_p_cat = km_multi_var(database, var, [0, np.ceil(uniroc_performance.loc[var, 'cutoff']),100], 'time', 'Outcome', 365, ci = False)
ax = plt.gca()
add_at_risk_counts(km_ef_p_cat['0 to 64.0']['kaplan_meier']['function'], km_ef_p_cat['64.0 to 100']['kaplan_meier']['function'], ax=ax)
plt.tight_layout()


# KM curves of peak gls with youden cutoff
var = 'GLS_P'
km_gls_p_cat = km_multi_var(database, var, [0, np.absolute(np.ceil( uniroc_performance.loc[var, 'cutoff'])), np.ceil( database[var].max())], 'time', 'Outcome', 365, ci = False)
ax = plt.gca()
add_at_risk_counts(km_gls_p_cat['0 to 17.0']['kaplan_meier']['function'], km_gls_p_cat['17.0 to 35.0']['kaplan_meier']['function'], ax=ax)
plt.tight_layout()


# Combine EF and GLS
var = 'pef_pgls'
km_ef_gls = km_multi_var(database, var, 2, 'time', 'Outcome', 365, ci = False)
ax = plt.gca()
add_at_risk_counts(km_ef_gls['0']['kaplan_meier']['function'], km_ef_gls['1']['kaplan_meier']['function'], ax=ax)
plt.tight_layout()


# KM curves for ischaemia
var = 'Ischaemic'
km_ischaemic = km_multi_var(database, var, 2, 'time', 'Outcome', 365, ci = False)
ax = plt.gca()
add_at_risk_counts(km_ischaemic['0']['kaplan_meier']['function'], km_ischaemic['1']['kaplan_meier']['function'], ax=ax)
plt.tight_layout()



#  cutoffs for this are chosen based on the best COs of combining ef and gls, rather than ther best COs individually. This is defined on line 357
database.loc[(database.Ischaemic == 0) & ((database.GLS_P_neg < -18) & (database.EF_P >  63)), 'ai_ischaemic'] = 0
# non-ischaemic with abnormal ai function
database.loc[(database.Ischaemic == 0) & ((database.GLS_P_neg >  -18) | (database.EF_P <  63)), 'ai_ischaemic'] = 1
# ischaemic with normal AI function
database.loc[(database.Ischaemic == 1) & ((database.GLS_P_neg < -18) & (database.EF_P >  63)), 'ai_ischaemic'] = 2
# ischemic with abnormal ai function
database.loc[(database.Ischaemic == 1) & ((database.GLS_P_neg > -18) | (database.EF_P < 63)), 'ai_ischaemic'] = 3

var = 'ai_ischaemic'
km_ai_ischaemic_n = km_multi_var(database, var, [-0.5,0.5,1.5,2.5, 3.5], 'time', 'Outcome', 366, ci = False)
ax = plt.gca()

rename_dict = {'-0.5 to 0.5': 'Non-ischaemic, normal AI systolic function', '0.5 to 1.5' : 'Non-ischaemic, abnormal AI systolic function', '1.5 to 2.5' : 'Ischaemic, normal AI systolic function', '2.5 to 3.5' : 'Ischaemic, abnormal AI systolic function'}
for i in rename_dict.keys():
    km_ai_ischaemic_n[rename_dict[i]] = km_ai_ischaemic_n.pop(i)

add_at_risk_counts(km_ai_ischaemic_n['Non-ischaemic, normal AI systolic function']['kaplan_meier']['function'], km_ai_ischaemic_n['Non-ischaemic, abnormal AI systolic function']['kaplan_meier']['function'], km_ai_ischaemic_n['Ischaemic, normal AI systolic function']['kaplan_meier']['function'], km_ai_ischaemic_n['Ischaemic, abnormal AI systolic function']['kaplan_meier']['function'], ax=ax)
ax.get_legend().remove()
plt.tight_layout()


# %% regression modelling

univars = ['Age', 'Gender', 'Height', 'Weight', 'BMI',  'HTN', 'Hypercholesterolaemia', 'Diabetes', 'Ischaemic', '3ischaemic', 'GLS_R_neg','GLS_P_neg','GLS_diff','EF_R','EF_P','EF_diff']
uv_logit = {}
univariable_regression_results = pd.DataFrame()
for i in univars:
    r, OR, CI, pval, p, n = logistic_regression(database, [i], 'Outcome', False)
    uv_logit.update({i : {'outcome' : 'CAD', 'odds ratio' : OR[1], 'll' : CI.iloc[1,0], 'ul' : CI.iloc[1,1], 'pval' : pval[1], 'performance' : p, 'sample size' : n}})
univariable_regression_results = pd.concat([univariable_regression_results, pd.DataFrame.from_dict(uv_logit).T], axis = 0)



mv_var = ['Hypercholesterolaemia', 'Diabetes', 'Ischaemic']
r, OR, CI, pval, p, n = logistic_regression(database, mv_var, 'Outcome', False)
mv_logit = {'outcome' : 'CAD', 'odds ratio' : OR, 'll' : CI.iloc[:,0], 'ul' : CI.iloc[:,1], 'pval' : pval[1], 'performance' : p, 'sample size' : n}

multivariable_regression_results = pd.DataFrame.from_dict(mv_logit)


m1diff_bootstrap= bootstrap_logistic_regression(database, mv_var, 'Coronary Intervention', False, 1000)

m2diff_bootstrap= bootstrap_logistic_regression(database, mv_var + ['EF_P_neg'], 'Coronary Intervention', False, 1000)

m3diff_bootstrap= bootstrap_logistic_regression(database, mv_var + ['GLS_P_neg'], 'Coronary Intervention', False, 1000)

